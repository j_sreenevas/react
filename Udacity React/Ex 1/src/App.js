import React, { Component } from 'react';
import logo from './logo.svg';
import Score from './Score';
import Game from './Game';
import './App.css';


class App extends Component {

{/* Added a state to app component*/ }
state = {
  correctAnswer: 0,
  numQuestions: 0,
};

{/* Update state */ }
handleAnswer = answerwascorrect => {

  if (answerwascorrect) {
    this.setState(currState => (
      {
        //Update Corrent Answer
        correctAnswer: currState.correctAnswer + 1
      }
    ))
  };

  this.setState(currState => (
    {
      //Updated Questions
      numQuestions: currState.numQuestions + 1
    }
  ))

};

render() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1 className="App-title">ReactND - Coding Practice</h1>
      </header>

      <div className="game">
        <h2>Mental Math</h2>
        {
          //
          //
        }
        <Game handleAnswer={this.handleAnswer} />
        {
          // Functional component 
          // Single responsibility
        }
        <Score noofQuestions={this.state.numQuestions} noofCorrect={this.state.correctAnswer} />
      </div>
    </div>
  );
}
}

export default App;

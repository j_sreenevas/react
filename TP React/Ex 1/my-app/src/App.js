import React from 'react';

class App extends React.Component {
  render() 
  {
   return (
      <div>
        <Header/>
        <Content/>
      </div>
   );
  }
}  

class Header extends React.Component {
  render() 
  {
   return (
      <div>
        <h2>Header</h2>
      </div>
   );
  }
}  

class Content extends React.Component {
  render() 
  {
   return (
      <div>
        <h2>Content</h2>
      </div>
   );
  }
}  



export default App;
